# Dockerfile for PHP

FROM php:7-fpm-alpine

# ---- Make "user-data" folder accessible ----
RUN mkdir -pv /var/www/html/user-data && chown www-data:www-data /var/www/html/user-data && chmod 775 /var/www/html/user-data
# RUN mkdir -pv /var/www/html/user-data && chown 33:33 /var/www/html/user-data
# RUN mkdir -pv /var/www/html/user-data && chown 82:82 /var/www/html/user-data
VOLUME "/var/www/html/user-data"
# RUN ls -la /var/www/html/

# ---- Install build utils ----
RUN apk add bash
RUN apk add make
RUN apk add gcc-avr
RUN apk add avr-libc

