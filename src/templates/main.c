/**
 * Blocktinu Project
 * @created {{date_time}}
 * @author {{author}}
 * This template is part of the Tinusaur/Blocktinu project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) {{copyright_year}} Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/blocktinu
 */

// ----------------------------------------------------------------------------

{{code}}

// ----------------------------------------------------------------------------
