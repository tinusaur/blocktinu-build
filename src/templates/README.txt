Blocktinu Project

Created {{date_time}}
Author {{author}}
Copyright (c) {{copyright_year}} by {{author}}

To build the project, type:
	$ make

To upload the HEX file into the microcontroller, type:
	$ make program

To clean up files left out from previous builds, type:
	$ make clean

