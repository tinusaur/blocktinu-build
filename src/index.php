<?php

/**
 * Blocktinu
 *
 * This is part of the Tinusaur/Blocktinu project.
 *
 * Copyright (c) 2018 The Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/blocktinu
 *
 */

// Check if the request method is OPTIONS
// if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
	// header('Access-Control-Allow-Credentials: true');
	// header('Content-Length: 0');
	// header('Content-Type: text/plain');
	// http_response_code(204); // Set the response code to 204 No Content
	// exit;
// }

// Set CORS headers
// Ref: https://en.wikipedia.org/wiki/Cross-origin_resource_sharing Cross-origin resource sharing
// Ref: https://stackoverflow.com/questions/298745/ How do I send a cross-domain POST request via JavaScript
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization');
// TODO: Remove for production environment.

header('Content-Type: application/json;charset=utf-8');

// define('DOC_ROOT', dirname(__FILE__) . '/');
define('USERDATA_DIR', '../user-data/');
define('TEMPLATES_DIR', './templates/');
define('LIBRARIES_DIR', './libraries/');
date_default_timezone_set('America/New_York'); //or change to whatever timezone you want

// ==== Debugging =============================================================
// /*DEBUG*/ print 'DEBUG: _SERVER ' . print_r($_SERVER, true) . '<br />\n';
// /*DEBUG*/ print "DEBUG: exec/whoami '" . exec('whoami') . "'<br />\n";
// /*DEBUG*/ print "DEBUG: getcwd '" . getcwd() . "'<br />\n";
$DEBUG_LOG = "DEBUG_LOG:\n";

// Extract source code files from the REQUEST.
if (isset($_POST['files'])) {
	$files_main_c = $_POST['files']['main.c'];
	// TODO: Process the rest of the files.
} else {
	if (isset($_POST['src'])) $files_main_c = $_POST['src'];
	/*DEBUG*/ if (!$src) $files_main_c = $_GET['src'];
}
// Render "main.c" source file template
$tpl_main_c = file_get_contents(TEMPLATES_DIR . 'main.c');
$files_main_c_source = strtr($tpl_main_c, array(
	'{{code}}' => $files_main_c,
	'{{date_time}}' => date('Y-m-d'),
	'{{copyright_year}}' => date('Y'),
	'{{author}}' => 'Unknown Author',
));
// TODO: Update "required libraries" in the main.c - some are added the the "Include handling".
// TODO: Render README files, if available.

$libs = isset($_POST['libs']) ? $_POST['libs'] : '';
/*DEBUG*/ if (!$libs) $libs = $_GET['libs']; // Get libs from a GET request too.
$libs_array = str_getcsv($libs);

$type = isset($_POST['type']) ? $_POST['type'] : '';
// ==== Includes ==============================================================
if ($type == 'clang') {
	// First, find lines with "#include ..." in $files_main_c
	$lines = explode("\n", $files_main_c); // Split into lines.
	foreach ($lines as $line) { // Loop through the lines.
		if (strpos($line, '#include') === 0) { // Check if starts with ...
		    if (preg_match('/"(.*?)"/', $line, $matches)) {
				$line_match = $matches[1];
				$line_parts = explode("/", $line_match); // Split into parts.
				$line_folder = $line_parts[0];
				$line_file = $line_parts[1];
				$line_mod = substr($line_file, 0, -2); // Get module name
				if ($line_folder == $line_mod) // Check if folder and file have the same name. Ex.: "ssd1306xled/ssd1306xled.h"
					// Replace "_" with "-" in the library name - for consistency in naming.
					$line_lib = str_replace("_", "-", $line_folder);
				else
					$line_lib = $line_folder . '-' . $line_mod;
				// /*DEBUG*/ $DEBUG_LOG .= "line_match: {$line_match} -> {$line_folder} / {$line_mod} ==> '{$line_lib}'\n";
				if (!in_array($line_lib, $libs_array)) $libs_array[] = $line_lib;
			}
		}
	}	
}

// ==== Libraries =============================================================
// Create the working project folder
$index = 0;
$date = date_create();
$project_folders = date_format($date, 'Ymd/His-u');
while (file_exists(USERDATA_DIR . $project_folders . $index)) $index++; // Repeat, increasing the index, if such folder already exists.
$project_folder_name = $project_folders . $index;
$project_folder_path = USERDATA_DIR . $project_folder_name;
// ==== Libraries Process =====================================================
$libs_array = array_combine($libs_array, $libs_array); // Copy values of the array to its keys.
// /*DEBUG*/ $DEBUG_LOG .= "libs_array: " . print_r($libs_array, true) . "\n";
$extlibsources = '';
array_walk($libs_array, 
    function (&$lib, $key) use (&$libs_array, $project_folder_path, &$extlibsources) {
		$libname_validate_regex = '/^([a-z0-9-_ ]+)$/i';
        static $count = 0; // Counter - protection for infinite (or too long) loops, in case of bugs.
        if ($count++ > 100) return; // The max num of libraries, could be increased.
        // /*DEBUG*/ echo $count . ") key/lib: $key\t $lib\n";
		// /*DEBUG*/ $libs_array += [ "$count.1" => "$lib.1" ];
        // /*DEBUG*/ echo "libs_array: " . print_r($libs_array, true) . "\n";
		// /*DEBUG*/ $GLOBALS['DEBUG_LOG'] .= "libs_array: " . implode(',', $libs_array) . "\n";
		if (preg_match($libname_validate_regex, $lib)) { // Validate lib name format. The $lib variable holds the library name.
			$lib_ini = parse_ini_file(LIBRARIES_DIR . $lib . '/' . $lib . '.ini', true);
			// /*DEBUG*/ echo "lib_ini: " .print_r($lib_ini, true);
			// ---- Lib folder setup ----
			$lib_folder = $lib_ini['folder'];
			if (!$lib_folder) return; // TODO: Validate folder name also
			$project_folder_path_lib = $project_folder_path . '/' . $lib_folder . '/';
			// /*DEBUG*/ echo "project_folder_path_lib: $project_folder_path_lib\n";
			if (!file_exists($project_folder_path_lib)) {
				$old_mask = umask(0);
				mkdir($project_folder_path_lib, 0755, true);
				umask($old_mask);
			}
			// ---- Process the FILES section ----
			if (!$lib_ini['files']) return;
			foreach ($lib_ini['files'] as $lib_files_group_key => $lib_files_group) {
				// /*DEBUG*/ echo "lib_files_group_key: $lib_files_group_key\n";
				// /*DEBUG*/ echo "lib_files_group: $lib_files_group\n";
				$lib_files = str_getcsv($lib_files_group);
				// /*DEBUG*/ echo "lib_files: " .print_r($lib_files, true);
				foreach ($lib_files as $lib_file) {
					// /*DEBUG*/ echo "lib_file: $lib_file\n";
					$lib_file_path = LIBRARIES_DIR . $lib . '/' . $lib_file;
					// /*DEBUG*/ echo "lib_file_path: $lib_file_path\n";
					if (is_file($lib_file_path)) {
						copy($lib_file_path, $project_folder_path_lib . $lib_file); // Copy lib file to the User-Data folder
						if ($lib_files_group_key == 'sources') {
							$extlibsources .= $lib_folder . '/' . $lib_file . ' '; // Add to EXTLIBSOURCES
						}
					} // ELSE: Skip it.
				}
			}
			// ---- Process the REQUIRE section ----
			if (!$lib_ini['require']) return;
			if (!$lib_ini['require']['libs']) return;
			$lib_require_libs = str_getcsv($lib_ini['require']['libs']);
			// /*DEBUG*/ echo "lib_require_libs: " . print_r($lib_require_libs, true);
			foreach ($lib_require_libs as $lib_require_lib) {
				$libs_array += [ $lib_require_lib => $lib_require_lib ];
			}
		}
    });
// /*DEBUG*/ echo "libs_array: " . print_r($libs_array, true) . "\n";
// /*DEBUG*/ echo "extlibsources: $extlibsources\n";
/*DEBUG*/ $DEBUG_LOG .= "libs_array: " . implode(',', $libs_array) . "\n";
/*DEBUG*/ $DEBUG_LOG .= "extlibsources: $extlibsources\n";
// ==== Build Template ========================================================
$makefile_contents = file_get_contents(TEMPLATES_DIR . 'Makefile');
$makefile_text = strtr($makefile_contents, array(
   '{{EXTLIBSOURCES}}' => $extlibsources,
));
// ---- Copy Template files ----
$template_files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(TEMPLATES_DIR), RecursiveIteratorIterator::SELF_FIRST);
foreach ($template_files as $template_file) {
	/* @var $template_file SplFileObject */
	if ($template_file->isDir()) {
		continue; // Skip if it is folder.
	}
	$file_name = $template_file->getFilename();
	$relative_file_path = substr($template_file->getPathname(), strlen(TEMPLATES_DIR));
	$dest_file = $project_folder_path . '/src/' . $relative_file_path;
	$pi = pathinfo($dest_file);
	$old_mask = umask(0);
	if ($pi['dirname'] && !file_exists($pi['dirname'])) {
		mkdir($pi['dirname'], 0755, true);
	}
	umask($old_mask);
	if ($file_name == 'main.c') {
		file_put_contents($dest_file, $files_main_c_source); // Replace the file with the generated content.
	} else if ($file_name == 'Makefile') { // Replace the file with the generated content.
		file_put_contents($dest_file, $makefile_text); // Replace the file with the generated content.
	} else if ($file_name == 'main.hex') {
		continue; // Skip the HEX file, if it exists in the template for some reason.
	} else {
		copy($template_file->getPathname(), $dest_file); // Copy file into the working folder.
	}
}

// Process response
chdir($project_folder_path);
/*DEBUG*/ file_put_contents('DEBUG_REQUEST.txt', var_export($_REQUEST, true));

// ---- Build ----
chdir('src/');
$result = shell_exec('chmod u+x build.sh');
$result = shell_exec('./build.sh');
// TODO: Check if make succeeded. Note: the "make" may not be installed at all.
$debug = ob_get_clean();
$response_data = array (
		// 'status' => 'success',
		// 'message' => 'Some custom message',
		'exec-result' => $result,
		// 'debug-result' => $debug,
	);
/*DEBUG*/ $response_data['server_id'] = $_SERVER['HTTP_HOST'] . ',' . $_SERVER['HOSTNAME'];
		 
$hexfile_filename = 'main.hex';
$build_log_filename = 'build.log';
$build_err_filename = 'build.err';

$response_data['header_offset'] = 18;
$response_data['footer_offset'] = 2;
$response_data['debug_test'] = 'debug-test';
if (!file_exists($hexfile_filename)) {
	$response_data['success'] = false;
	$response_data['message'] = 'HEX file does not exist';
} else {
	$response_data['success'] = true;
	$response_data['bthex'] = base64_encode(file_get_contents($hexfile_filename));
}
if (file_exists($build_log_filename)) $response_data['build_log'] = base64_encode(file_get_contents($build_log_filename));
if (file_exists($build_err_filename)) $response_data['build_err'] = base64_encode(file_get_contents($build_err_filename));

echo json_encode($response_data);
chdir('../');
/*DEBUG*/ file_put_contents('DEBUG_LOG.txt', $DEBUG_LOG);
exit;
