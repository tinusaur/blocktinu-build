/**
 * Blocktinu - TESTING
 *
 * @created 2017-05-12
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Blocktinu project.
 *
 * Copyright (c) 2018 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/blocktinu
 *
 */

// ============================================================================


#include "testing/testing.h"
#include "testing2/testing2a.h"

// ----------------------------------------------------------------------------

uint8_t blocktinu_testing_function2a(void) {
	if (blocktinu_testing_variable == 0)
		blocktinu_testing_variable = 1;
	return blocktinu_testing_variable++;
}

// ============================================================================
