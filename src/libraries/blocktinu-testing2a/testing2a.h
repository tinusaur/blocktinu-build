/**
 * Blocktinu - TESTING
 *
 * @created 2017-05-12
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Blocktinu project.
 *
 * Copyright (c) 2018 Neven Boyanov, Tinusaur Team. All Rights Reserved.
 * Distributed as open source software under MIT License, see LICENSE.txt file.
 * Retain in your source code the link http://tinusaur.org to the Tinusaur project.
 *
 * Source code available at: https://bitbucket.org/tinusaur/blocktinu
 *
 */

// ----------------------------------------------------------------------------

#ifndef BLOCKTINU_TESTING2A_H
#define BLOCKTINU_TESTING2A_H

// ----------------------------------------------------------------------------

#include <stdint.h>

// ----------------------------------------------------------------------------

uint8_t blocktinu_testing_function2a(void);

// ----------------------------------------------------------------------------

#endif

// ----------------------------------------------------------------------------
