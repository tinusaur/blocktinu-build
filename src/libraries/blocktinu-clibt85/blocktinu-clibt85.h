/**
 * Blocktinu - CLibT85
 *
 * @created 2017-05-12
 * @author Neven Boyanov
 *
 * This is part of the Tinusaur/Blocktinu project.
 *
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2024 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur
 */

// ============================================================================
// **** NOTE: THIS LIBRARY IS NO LONGER IN USE ****
// ============================================================================

#ifndef BLOCKTINU_CLIBT85_H
#define BLOCKTINU_CLIBT85_H

#endif

// ============================================================================
