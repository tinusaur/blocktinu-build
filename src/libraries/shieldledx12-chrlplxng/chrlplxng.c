/**
 * Shield-LEDx12-Charlieplexing - Tinusaur Shield LEDx12 Charlieplexing Library
 * @author Neven Boyanov
 * This is part of the Tinusaur/Shield-LEDx12 project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2021 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/shield-ledx12
 */

// ============================================================================

#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "chrlplxng.h"

// ----------------------------------------------------------------------------

// Data

const uint8_t chrlplxng_leds[] PROGMEM = {
	CHRLPLXNG_LED01, CHRLPLXNG_LED02, CHRLPLXNG_LED03,
	CHRLPLXNG_LED04, CHRLPLXNG_LED05, CHRLPLXNG_LED06,
	CHRLPLXNG_LED07, CHRLPLXNG_LED08, CHRLPLXNG_LED09,
	CHRLPLXNG_LED10, CHRLPLXNG_LED11, CHRLPLXNG_LED12,
};

// ----------------------------------------------------------------------------

void chrlplxng_led(uint8_t i) {
	uint8_t b = pgm_read_byte(&chrlplxng_leds[i]);
	DDRB = ((DDRB & 0xf0) | (b >> 4));
	PORTB = ((PORTB & 0xf0) | (b & 0x0f));
}

void chrlplxng_off(void) {
	DDRB = DDRB & 0xf0;
	PORTB = PORTB & 0xf0;
}

// ============================================================================
