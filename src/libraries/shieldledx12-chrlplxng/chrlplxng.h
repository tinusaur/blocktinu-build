/**
 * Shield-LEDx12-Charlieplexing - Tinusaur Shield LEDx12 Charlieplexing Library
 * @author Neven Boyanov
 * This is part of the Tinusaur/Shield-LEDx12 project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2021 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/shield-ledx12
 */

// ============================================================================

#ifndef CHRLPLXNG_H
#define CHRLPLXNG_H

// ----------------------------------------------------------------------------

#include <stdint.h>

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~ Shield-LEDx12-Charlieplexing ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//         Shield LEDx12 Charlieplexing
//               +----------+
//      (RST)--> +      Vcc +---(+)--VCC--
// ---------A3---+ PB3  PB2 +---A2--------
// --------------+ PB4  PB1 +---A1--------
// --------(-)---+ GND  PB0 +---A0--------
//               +----------+
//                 Tinusaur
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Definitions

#define CHRLPLXNG_LEDS_SIZE	12

#define CHRLPLXNG_LED01	((uint8_t) (~0b0110 << 4) | (0b1000))
#define CHRLPLXNG_LED02	((uint8_t) (~0b0101 << 4) | (0b1000))
#define CHRLPLXNG_LED03	((uint8_t) (~0b0011 << 4) | (0b1000))
#define CHRLPLXNG_LED04	((uint8_t) (~0b1010 << 4) | (0b0100))
#define CHRLPLXNG_LED05	((uint8_t) (~0b1001 << 4) | (0b0100))
#define CHRLPLXNG_LED06	((uint8_t) (~0b0011 << 4) | (0b0100))
#define CHRLPLXNG_LED07	((uint8_t) (~0b1100 << 4) | (0b0010))
#define CHRLPLXNG_LED08	((uint8_t) (~0b1001 << 4) | (0b0010))
#define CHRLPLXNG_LED09	((uint8_t) (~0b0101 << 4) | (0b0010))
#define CHRLPLXNG_LED10	((uint8_t) (~0b1100 << 4) | (0b0001))
#define CHRLPLXNG_LED11	((uint8_t) (~0b1010 << 4) | (0b0001))
#define CHRLPLXNG_LED12	((uint8_t) (~0b0110 << 4) | (0b0001))

// ----------------------------------------------------------------------------

void chrlplxng_led(uint8_t i);
void chrlplxng_off(void);

// ----------------------------------------------------------------------------

#endif

// ============================================================================
