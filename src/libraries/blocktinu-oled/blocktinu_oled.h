/**
 * Blocktinu-CLibT85 - OLED Library
 * @author Neven Boyanov
 * This is part of the Tinusaur/Blocktinu-CLibT85 project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2021 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/blocktinu-clibt85
 */

// ============================================================================

#ifndef BLOCKTINU_OLED_H
#define BLOCKTINU_OLED_H

// ----------------------------------------------------------------------------

#include <stdint.h>

#include "ssd1306xled/ssd1306xled.h"

// ----------------------------------------------------------------------------

void oled_init(void);

// ----------------------------------------------------------------------------

#define oled_clear() ssd1306_fill(0)
#define oled_fill() ssd1306_fill(0xff)

void oled_string(uint8_t, uint8_t, char *);

// ----------------------------------------------------------------------------

void oled_nump(uint8_t, uint8_t, uint16_t);

// ----------------------------------------------------------------------------

#endif

// ============================================================================
