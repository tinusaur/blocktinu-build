/**
 * Blocktinu-CLibT85 - OLED Lbrary
 * @author Neven Boyanov
 * This is part of the Tinusaur/Blocktinu-CLibT85 project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2021 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/blocktinu-clibt85
 */

// ============================================================================

#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "tinyavrlib/num2str.h"
#include "ssd1306xled/ssd1306xled.h"

#include "blocktinu_oled.h"
#include "blocktinu_font8x8.h"

// ----------------------------------------------------------------------------

void oled_init(void) {
	ssd1306_init();
}

// ----------------------------------------------------------------------------

void oled_ch(char ch) {
	uint16_t j = (ch << 3) - 256; // Equiv.: j=(ch-32)*8 <== Convert ASCII code to font data index.
	ssd1306_start_data();
	for (uint8_t i = 8; i > 0; i--) {
		ssd1306_byte(pgm_read_byte(&oled_font8x8data[j++]));
	}
	ssd1306_stop();
}

void oled_string(uint8_t x, uint8_t y, char *s) {
	ssd1306_setpos(x << 3, y);
	while (*s) {
		oled_ch(*s++);
	}
}

// ----------------------------------------------------------------------------

char oled_numdec_buffer[USINT2DECASCII_MAX_DIGITS + 1];
void oled_nump(uint8_t x, uint8_t y, uint16_t num) {
	oled_numdec_buffer[USINT2DECASCII_MAX_DIGITS] = '\0';   // Terminate the string.
	usint2decascii(num, oled_numdec_buffer);
	oled_string(x, y, oled_numdec_buffer);
}

// ============================================================================
