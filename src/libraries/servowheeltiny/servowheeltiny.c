/**
 * ServoWheelTiny - The ServoWheelTiny Project library.
 * @author Neven Boyanov
 * This is part of the Tinusaur/ServoWheelTiny project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2022 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/servowheeltiny
 */

// ============================================================================

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "tinyavrlib/scheduler.h"
#include "servowheeltiny/servowheeltiny.h"

// ----------------------------------------------------------------------------

// Motors I/O ports
// static uint8_t servowheel_mot1_port;
// static uint8_t servowheel_mot2_port;
// NOTE/TEST: Using variable for IO port => 808 bytes of code
// NOTE/TEST: Using DEFINE for IO port => 614 bytes of code

// Motors speed and direction settings
static uint8_t servowheel_mot1 = 0;
static uint8_t servowheel_mot2 = 0;

// ----------------------------------------------------------------------------

void servowheel_init(void) {
	// Setup PORTs
	// servowheel_mot1_port = mot1_port;
	// servowheel_mot2_port = mot2_port;
	DDRB |= (1 << SERVOWHEEL_MOT1_PORT); // Set port as output
	DDRB |= (1 << SERVOWHEEL_MOT2_PORT); // Set port as output
	scheduler_usertask(servowheel_scheduler_task, 0);
	// NOTE/IMPORTANT: For this library to work the Scheduler MUST BE set at 1 ms tick interval.
}

void servowheel_scheduler_task(void) {
	static uint8_t s;	// Static status counter - initial value is 0.
	if (s == 0) {
		PORTB |= (1 << SERVOWHEEL_MOT1_PORT);	// Hi
		PORTB |= (1 << SERVOWHEEL_MOT2_PORT);	// Hi
	}
	if (s == 1) {
		if (servowheel_mot1 < servowheel_mot2) {
			//----MOT1..----MOT2......
			for (uint8_t i = 0; i <= servowheel_mot2; i++) {
				_delay_us(1);
				if (i == servowheel_mot1)
					PORTB &= ~(1 << SERVOWHEEL_MOT1_PORT);	// Lo
			}
			PORTB &= ~(1 << SERVOWHEEL_MOT2_PORT);	// Lo
		} else if (servowheel_mot1 >= servowheel_mot2) {
			//----MOT2..----MOT1......
			for (uint8_t i = 0; i <= servowheel_mot1; i++) {
				_delay_us(1);
				if (i == servowheel_mot2)
				PORTB &= ~(1 << SERVOWHEEL_MOT2_PORT);	// Lo
			}
			PORTB &= ~(1 << SERVOWHEEL_MOT1_PORT);	// Lo
		}
	}
	if (++s > 19) s = 0; // Increment & Check boundaries.
}

// Forward: 32	Stopped: 0	Reverse: -32
// Values MUST NOT be less that -32 or more that 32.
void servowheel_motors(int8_t mot1, int8_t mot2) {
	servowheel_mot1 = 36 - mot1;
	if (servowheel_mot1 > 80) servowheel_mot1 = 80;
	servowheel_mot2 = 36 + mot2;
	if (servowheel_mot2 > 80) servowheel_mot2 = 80;
	// servowheel_motors 1,2 Values
	//   Stopped: 42...38(1.50ms)...32
	//   Max: 80
	//   Min: 0
	// servowheel_mot1 = 80;
	// servowheel_mot2 = 80;
}

// ============================================================================
