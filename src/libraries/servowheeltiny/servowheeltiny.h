/**
 * ServoWheelTiny - The ServoWheelTiny Project library.
 * @author Neven Boyanov
 * This is part of the Tinusaur/ServoWheelTiny project.
 * ----------------------------------------------------------------------------
 *  Copyright (c) 2022 Tinusaur (https://tinusaur.com). All rights reserved.
 *  Distributed as open source under the MIT License (see the LICENSE.txt file)
 *  Please, retain in your work a link to the Tinusaur project website.
 * ----------------------------------------------------------------------------
 * Source code available at: https://gitlab.com/tinusaur/servowheeltiny
 */

// ============================================================================

#ifndef SERVOWHEELTINY_H
#define SERVOWHEELTINY_H

// ----------------------------------------------------------------------------

#include <stdint.h>

// ----------------------------------------------------------------------------

#define SERVOWHEEL_MOT1_PORT PB0
#define SERVOWHEEL_MOT2_PORT PB1

// ----------------------------------------------------------------------------

void servowheel_init(void);
void servowheel_scheduler_task(void);
void servowheel_motors(int8_t mot1, int8_t mot2);

// ----------------------------------------------------------------------------

#endif

// ============================================================================
