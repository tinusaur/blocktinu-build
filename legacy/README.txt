Blocktinu - Platform for visual learning and teaching of C/C++ for microcontrollers.

The Blocktinu is a platform for visual learning and teaching of C/C++ programming language 
and for writing application software for microcontrollers. It is intended for the beginners 
but could be used by more advanced user as well.

The Blocktinu platform is part of the Tinusaur project.

-----------------------------------------------------------------------------------
 Copyright (c) 2018 Neven Boyanov, The Tinusaur Team. All Rights Reserved.
 Distributed as open source software under MIT License, see LICENSE.txt file.
 Retain in your source code the link http://tinusaur.org to the Tinusaur project.
-----------------------------------------------------------------------------------

Official Tinusaur Project website: http://tinusaur.org
Project Blocktinu page: http://tinusaur.org/projects/blocktinu/
Project Blocktinu source code: https://bitbucket.org/tinusaur/blocktinu

Twitter: https://twitter.com/tinusaur
Facebook: https://www.facebook.com/tinusaur

