Install Blocktinu Build Webserver


---- OS Maintenance ----

Update the OS packages first:
	sudo apt-get update
	sudo apt-get upgrade

In some rare cases this may require a reboot.
	sudo reboot


---- Apache web server ----

The server requires to have the Apache web-server installed
	sudo apt-get install apache2

Create new conf file and insert the configuration
	sudo nano /etc/apache2/sites-available/blocktinu-eult1dev.conf

Sample web site configuration

	<VirtualHost *:80>

			ServerName eult1dev.BLOCKTINU.NET
			ServerAlias www.eult1dev.BLOCKTINU.NET
			ServerAdmin webmaster@localhost

			DocumentRoot /var/www/blocktinu-eult1dev
			<Directory /var/www/blocktinu-eult1dev/>
					Options Indexes FollowSymLinks MultiViews
					AllowOverride All
					Order allow,deny
					allow from all
			</Directory>

			ErrorLog ${APACHE_LOG_DIR}/blocktinu_error.log
			CustomLog ${APACHE_LOG_DIR}/blocktinu_access.log combined

	</VirtualHost>

Enable the site
	sudo a2ensite blocktinu-eult1dev

You may need to disable the default website if the hostname (in the OS) is the same as the new sitename
	sudo a2dissite 000-default

Reload the Apache service configuration
	sudo service apache2 reload

NOTE: make sure that the website folder exists in the file system.
	sudo mkdir /var/www/blocktinu-eult1dev/


---- PHP ----

Install PHP first
	sudo apt-get install php

Then, install the Apache2 module
	sudo apt-get install libapache2-mod-php


---- Install Version Control Software ----

You will need Mercurial (hg).
Install the Mercurial/hg
	sudo apt-get install mercurial
or
	sudo apt install mercurial


